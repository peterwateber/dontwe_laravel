@extends('admin.index')

@section('styles')
    @foreach($default_header_styles as $style)
        <link rel="stylesheet" href="{{$style}}" />
    @endforeach
@endsection

@section('scripts')
    @foreach($default_scripts as $script)
        <script src="{{ $script }}"></script>
    @endforeach
@endsection
