<?php load_view(ADMIN_INCLUDES_DIR, 'header'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Create an account</h3>
                </div>
                <div class="panel-body">
                    <form role="form">
                        <fieldset>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <?php
                                        echo form_input(array(
                                            'name' => 'name',
                                            'placeholder' => 'Name',
                                            'class' => 'form-control',
                                            'autofocus' => 'autofocus',
                                            'value' => set_value('name')
                                        ));
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php
                                echo form_input(array(
                                    'name' => 'screenname',
                                    'placeholder' => '@screenname',
                                    'class' => 'form-control',
                                    'autofocus' => 'autofocus',
                                    'value' => set_value('email')
                                ));
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                echo form_input(array(
                                    'name' => 'email',
                                    'placeholder' => 'Email',
                                    'class' => 'form-control',
                                    'autofocus' => 'autofocus',
                                    'value' => set_value('email')
                                ));
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                echo form_password(array(
                                    'name' => 'password[]',
                                    'placeholder' => 'Password',
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                echo form_password(array(
                                    'name' => 'password[]',
                                    'placeholder' => 'Confirm Password',
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <?php
                                    echo form_checkbox(array(
                                        'name' => 'cookie',
                                        'value' => '1',
                                        'checked' => FALSE
                                    ));
                                    ?>
                                    Remember Me
                                </label>
                            </div>
                            <input type="submit" class="btn btn-lg btn-success btn-block" value="Login" />
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php load_view(ADMIN_INCLUDES_DIR, 'footer'); ?>