<ul class="nav navbar-top-links navbar-right">
    @include('admin.includes.dropdown.notification')
    @include('admin.includes.dropdown.user')
</ul>