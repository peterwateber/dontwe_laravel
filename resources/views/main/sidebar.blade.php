<div class="sidebar-wrapper">
    <ul class="sidebar-nav">    
        <li class="sidebar-brand">
            <a class="menu-toggle" href="#" data-navbar>
                <span class="glyphicon glyphicon-align-justify"></span>
            </a>
        </li> 
        <li>
            <a href="#">
                <i class="fa fa-home"></i>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-th-large"></i>
            </a>
        </li>
    </ul>
</div>