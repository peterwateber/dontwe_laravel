@extends('main.account.login')

@section('meta')    
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
@endsection

@section('styles')
    @foreach($default_header_styles as $style)
        <link rel="stylesheet" href="{{$style}}" />
    @endforeach
@endsection

@section('scripts')
    @foreach($default_scripts as $style)
         <script src="{{$style}}"></script>
    @endforeach
@endsection