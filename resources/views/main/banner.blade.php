<div class="row">
    <div class="banner">
        <div class="social">
            <ul class="list-unstyled">
                <li>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                </li>
            </ul>
        </div>
        <h1 class="site-name">DONTWE</h1>
        <h2 class="headlines">Dream big but start small.</h2>
    </div>
</div>