            </div>
            <div class="footer">
                <div class="footer-wrapper">
                    Copyright &copy; 2015. 
                </div>
            </div>
        </div>
        @yield('scripts')
        <script>
            $("[data-navbar]").click(function(e) {
                e.preventDefault();
                $(".menu-icon").toggleClass("menu-hidden-icon");
                $(".wrapper").toggleClass("active");
            });
        </script>
    </body>
</html>
