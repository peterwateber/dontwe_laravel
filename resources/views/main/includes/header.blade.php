<!DOCTYPE html>
<html>
    <head>
        <title>{{ $title }}</title>
        @yield('meta')
        @yield('styles')
    </head>
    <body>
        @include('main.includes.navigator')
