<!DOCTYPE html>
<html>
    <head>
        <title>{{ $title }}</title>
        @yield('meta')
        @yield('styles')
    </head>
    <body>
        <div class="body-background"></div>
        <div class="body-overlay">
            <div class="bimg"></div>
            <div class="bfade"></div>
        </div>
        <div class="wrapper">
            @include('main.sidebar')