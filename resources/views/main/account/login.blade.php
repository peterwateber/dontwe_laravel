@include('main.includes.header')
<div class="container">
    <div class="row">
        <div class="col-sm-4 col-mg-4 col-lg-4 col-xs-offset-4 account">
            <div class="login">
                <form class="login-form">
                    <div class="form-group">
                        <label class="sr-only">Username</label>
                        <input type="text" class="form-control" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label class="sr-only">Password</label>
                        <input type="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group login-group-checkbox">
                        <label>
                            <input type="checkbox"> Remember me
                        </label>
                    </div>
                    <button type="submit" class="login-button">
                        <i class="fa fa-chevron-right"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@include('main.includes.footer')