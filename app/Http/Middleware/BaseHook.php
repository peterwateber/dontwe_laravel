<?php

namespace App\Http\Middleware;

use Closure;

class BaseHook {

    /**
     * Handle an incoming request.
     * DETECT FOR MIDDLEWARE BEFORE ROUTE.
     */
    
    public function handle($request, Closure $next) {
        $route_name = explode('.', $request->getHost());
        $method = $request->segment(1);
        if (is_array($route_name)) {
            if ($route_name[0] === 'admin' && $method !== 'account') {
                if (!$request->session()->has(SESSION_LOGIN_NAME)) {
                    return redirect()->route('admin_login');
                }
            }
        }
        return $next($request);
    }

}
