<?php

namespace App\Http\Middleware;

use Closure;

class AdminAccountHook {

    public function handle($request, Closure $next) {
        $method = $request->segment(2);
        if (!$request->session()->has(SESSION_LOGIN_NAME) ||
            !$request->cookie(COOKIE_LOGIN_NAME)) {
            /**
             * ALLOW CERTAIN LINKS FOR CREDENTIAL
             * TO A GUEST USER
             */
            switch ($method) {
                case 'forgot':
                case 'register':
                case 'login':
                    break;
                default:
                    return redirect('account/login');
            }
        } else {
            switch ($method) {
                case 'forgot':
                case 'register':
                case 'login':
                    return redirect()->route('admin_home');
            }
        }
        return $next($request);
    }

}
