<?php

namespace App\Http\Middleware;

use Closure;

class AccountHook {

    public function handle($request, Closure $next) {
        $method = $request->segment(2);
        if (!$request->session()->has(SESSION_LOGIN_NAME)) {
            /**
             * ALLOW CERTAIN LINKS FOR CREDENTIAL
             * TO A GUEST USER
             */
            switch($method) {
                case 'forgot':
                case 'register':
                case 'login':
                    break;
                default:
                    return redirect('account/login');
            }
        } else {
            switch($method) {
                case 'forgot':
                case 'register':
                case 'login':
                    redirect('/');
                    break;
            }
        }
        return $next($request);
    }

}
