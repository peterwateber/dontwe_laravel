<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class Base extends Controller {
    
    var $data;
    
    const JS_DIR = '/js/';
    const CSS_DIR = '/css/';
    
    
    function __construct() {
        $this->data['title'] = 'Dont We';
    }
}
