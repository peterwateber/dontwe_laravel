<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Base;

class MainController extends Base {

    var $data;

    function __construct() {
        $this->data = array(
            'css_dir' => self::CSS_DIR,
            'js_dir' => self::JS_DIR
        );

        $this->data['default_header_styles'] = array(
            'font' => 'http://fonts.googleapis.com/css?family=Montserrat:400,700|Libre+Baskerville:400,400italic',
            'bootstrap' => url() . self::CSS_DIR . 'bootstrap.min.css',
            'fa' => url() . self::CSS_DIR . 'fa.css',
            'style' => url() . self::CSS_DIR . 'style.css',
            'mobile' => url() . self::CSS_DIR . 'mobile.css',
        );
        
        $this->data['default_scripts'] = array(
            'jquery_cdn' => url() . self::JS_DIR . 'jquery.min.js',
            'bootstrap' => url() . self::JS_DIR . 'bootstrap.min.js'
        );
    }

    function index() {
        $this->data['title'] = 'Dont We';
        $this->data['default_scripts']['salvatore'] = url() . self::JS_DIR . 'salvatore.js';
        return View('index', $this->data);
    }

}
