<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Base;

class AdminController extends Base {

    var $data = array();

    function __construct() {
        $this->data['default_header_styles'] = array(
            'bootstrap' => url() . self::CSS_DIR . 'bootstrap.min.css',
            'admin' => url() . self::CSS_DIR . 'admin.css',
            'admin_mobile' => url() . self::CSS_DIR . 'admin.mobile.css',
            'fa' => url() . self::CSS_DIR . 'fa.css'
        );
        
        $this->data['default_scripts'] = array(
            'jquery' => url() . self::JS_DIR . 'jquery.min.js',
            'bootstrap' => url() . self::JS_DIR . 'bootstrap.min.js',
            'admin' => url() . self::JS_DIR . 'admin.js'
        );
    }

    function index() {
        $this->data['title'] = 'Dashboard';
        $this->data['project_name'] = 'Dont We';
        return view('admin', $this->data);
    }

    function dashboard() {
        echo 'dashboard';
    }
    
    function user() {
        $request->session()->put('login', TRUE);
    }

}
