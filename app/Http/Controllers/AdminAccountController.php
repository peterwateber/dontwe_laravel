<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use Session;

class AdminAccountController extends Base {

    var $data = array();
    
    function __construct() {
        parent::__construct();
        $this->data['default_header_styles'] = array(
            'bootstrap' => url() . self::CSS_DIR . 'bootstrap.min.css',
            'fa' => url() . self::CSS_DIR . 'fa.css',
            'main' => url() . self::CSS_DIR . 'main.css',
            'login' => url() . self::CSS_DIR . 'login.css',
        );
        
        $this->data['default_scripts'] = array(
            'jquery' => url() . self::JS_DIR . 'jquery.min.js',
            'bootstrap' => url() . self::JS_DIR . 'bootstrap.min.js',
            'admin' => url() . self::JS_DIR . 'admin.js'
        );
        $this->data['islogin'] = Session::has(SESSION_LOGIN_NAME);
    }
    
    function index() {
    }

    function login() {
        return View('main.login', $this->data);
    }

}
