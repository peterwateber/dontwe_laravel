<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use Session;

class AccountController extends Base {
    
    function __construct() {
        parent::__construct();
    }
    
    function index() {
    }

    function login() {

        $this->data['default_header_styles'] = array(
            'font' => 'http://fonts.googleapis.com/css?family=Montserrat:400,700|Libre+Baskerville:400,400italic',
            'bootstrap' => url() . self::CSS_DIR . 'bootstrap.min.css',
            'fa' => url() . self::CSS_DIR . 'fa.css',
            'main' => url() . self::CSS_DIR . 'main.css',
            'login' => url() . self::CSS_DIR . 'login.css'
        );
        
        $this->data['default_scripts'] = array(
            'jquery_cdn' => url() . self::JS_DIR . 'jquery.min.js',
            'bootstrap' => url() . self::JS_DIR . 'bootstrap.min.js'
        );
        return View('main.login', $this->data);
    }

}
