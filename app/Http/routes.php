<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::group(['domain' => APP_BASE_URL], function() {
    Route::any('/', ['as' => 'home', 'uses' => 'MainController@index']);

    Route::group(['prefix' => 'account', 'middleware' => 'account'], function() {
        Route::get('/', ['as' => 'account', 'uses' => 'AccountController@index']);
        Route::get('login', ['as' => 'normal_login', 'uses' => 'AccountController@login']);
    });
});

Route::group(['domain' => 'admin.' . APP_BASE_URL], function () {
    Route::any('/', ['as' => 'admin_home', 'uses' => 'AdminController@index']);

    Route::group(['prefix' => 'account', 'middleware' => 'adminaccount'], function() {
        Route::get('/', ['as' => 'admin_account', 'uses' => 'AdminAccountController@index']);
        Route::get('login', ['as' => 'admin_login', 'uses' => 'AdminAccountController@login']);
    });
});
